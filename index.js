const md5File = require('md5-file')
const path = require('path')

const fileList = [
    '/Users/wishdev/my_project/md5/index.js',
    '/Users/wishdev/my_project/md5/.gitignore',
    '/Users/wishdev/my_project/md5/package.json'
];

fileList.forEach((fileName) => {
    const hash = md5File.sync(fileName)
    console.log(path.basename(fileName));
    console.log(path.dirname(fileName));
    console.log(hash)
    console.log();
});
